# Brandon - 09/14/19
## Installation
 - With Docker:
   * docker-compose up
## Checklist
 - [x] Can run app within Docker without installing Node.js on host
 - [ ] Review App for app
 - [ ] Review App for test code coverage report
 
## Demo
**Merge Request:** CHANGE ME

**App Review App:** CHANGE ME

**Coverage Review App:** CHANGE ME
## Features
 - Quickly spin up a node react environment and get coding right away
 - Review and see changes with the Review App
## Security
 - Issues Not Addressed:
   * Local docker registry not handled on CI
## Improvements
---
## Other notes