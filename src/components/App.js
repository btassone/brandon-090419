import React from 'react'
import Header from '../containers/Header'
import MainSection from '../containers/MainSection'

const App = () => (
  <div>
    <Header />
    <MainSection />
  </div>
)

// Making a change so tests will run

export default App
